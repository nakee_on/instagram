import React from 'react'
import PropTypes from 'prop-types'
import ReactPlayer from 'react-player'
import Icon from '@fortawesome/react-fontawesome'

import Mark from 'components/Mark'
import Controls from './Controls'
import './Player.css'

class Player extends React.Component {
  static propTypes = {
    marksControls: PropTypes.bool,
    url: PropTypes.string.isRequired,
    start: PropTypes.number,
    end: PropTypes.number,
    onCrop: PropTypes.func,
    isCropped: PropTypes.bool,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    onDelete: PropTypes.func,
  }
  static defaultProps = {
    marksControls: true,
    start: 0,
    end: 0,
    onCrop: undefined,
    onDelete: undefined,
    isCropped: false,
    id: 'primary',
  }
  constructor(props) {
    super(props)

    this.state = {
      progress: props.start,
      duration: props.end,
      playing: false,
      muted: false,
      volume: 1,
      marks: {},
      markToRemove: null,
    }
  }
  setMarkToRemove = markToRemove => this.setState({ markToRemove })
  handleOnCrop = () => {
    const { marks } = this.state
    const { id } = this.props
    const marksKeys = Object.keys(marks)

    const cropped = {
      start: parseFloat(marksKeys[0]),
      end: parseFloat(marksKeys[1]),
    }

    this.props.onCrop(cropped, id)

    this.setState({ marks: {} })
  }
  handleOnDelete = () => this.props.onDelete(this.props.id)
  handleOnProgress = ({ playedSeconds }) => {
    const { isCropped, start, end } = this.props

    if (isCropped && playedSeconds >= end && this.state.playing) {
      this.player.seekTo(start)
      this.setState({ progress: start, playing: false })
    } else {
      this.setState({ progress: playedSeconds })
    }
  }
  handleOnPlayerReady = () => {
    this.setState({ duration: this.player.getDuration() })
  }
  handleOnPlay = () => {
    const { isCropped, start } = this.props
    const { progress } = this.state

    if (isCropped && progress < start) {
      this.player.seekTo(start)
    }
    this.setState({ playing: true })
  }
  handleOnPause = () => this.setState({ playing: false })
  handleOnSliderChange = progress => this.setState({ progress })
  handleOnSliderAfterChange = value => this.player.seekTo(value)
  handleOnPlayToggle = () =>
    this.setState(state => ({ playing: !state.playing }))
  handleOnMuteToggle = () => this.setState(state => ({ muted: !state.muted }))
  handleOnMarkCreate = () =>
    this.setState((state) => {
      if (state.marks[state.progress]) {
        return null
      }

      if (Object.keys(state.marks).length === 2) {
        return null
      }

      if (Object.keys(state.marks).length === 1) {
        return {
          marks: {
            ...state.marks,
            [state.progress]: {
              label: (
                <Mark value={state.progress} onClick={this.setMarkToRemove} />
              ),
            },
          },
        }
      }

      return {
        marks: {
          [state.progress]: {
            label: (
              <Mark value={state.progress} onClick={this.setMarkToRemove} />
            ),
          },
        },
      }
    })
  handleOnMarkRemove = () =>
    this.setState((state) => {
      const newMarks = { ...state.marks }
      delete newMarks[state.markToRemove]

      return { marks: newMarks, markToRemove: null }
    })
  render() {
    const {
      marks,
      playing,
      muted,
      volume,
      duration,
      progress,
      markToRemove,
    } = this.state
    const { url, isCropped } = this.props
    return (
      <div className="player-container">
        <ReactPlayer
          ref={(player) => {
            this.player = player
          }}
          muted={muted}
          volume={volume}
          playing={playing}
          onPlay={this.handleOnPlay}
          onPause={this.handleOnPause}
          onReady={this.handleOnPlayerReady}
          onProgress={this.handleOnProgress}
          width="100%"
          height="100%"
          url={url}
        />
        {url && (
          <Controls
            playing={playing}
            muted={muted}
            start={this.props.start}
            duration={this.props.end || duration}
            marks={marks}
            progress={progress}
            markToRemove={markToRemove}
            marksControls={this.props.marksControls}
            onSliderChange={this.handleOnSliderChange}
            onPlayToggle={this.handleOnPlayToggle}
            onMarkRemove={this.handleOnMarkRemove}
            onMarkCreate={this.handleOnMarkCreate}
            onSliderAfterChange={this.handleOnSliderAfterChange}
            onMuteToggle={this.handleOnMuteToggle}
            onCrop={this.handleOnCrop}
          />
        )}
        {isCropped && (
          <Icon
            icon="times"
            color="#fff"
            onClick={this.handleOnDelete}
            className="remove-preview-icon"
            size="lg"
          />
        )}
      </div>
    )
  }
}

export default Player
