import os
import random
from pathlib import Path

from celery import shared_task
from django.conf import settings
from InstagramAPI import InstagramAPI
from moviepy.editor import VideoFileClip
from pytube import YouTube


@shared_task
def crop_and_deploy(video_url, previews):
    instagram = InstagramAPI(
        settings.INSTAGRAM_LOGIN, settings.INSTAGRAM_PASSWORD)

    # get YouTube video id
    video_id = video_url.split('=')[-1]
    video_file = Path(os.path.join(
        settings.BASE_DIR, "{}.mp4".format(video_id)))

    if not video_file.is_file():
        yt = YouTube(video_url)

        stream = yt.streams.filter(subtype='mp4').first()
        stream.download(filename=video_id)

    instagram.login()

    for preview in previews:
        # create unique id for video
        chars = []
        for i in range(5):
            chars.append(random.choice(settings.SECRET_KEY))

        video_salt = "".join(chars)

        # crop video
        video = VideoFileClip("%s.mp4" % video_id).subclip(
            preview['start'], preview['end'])

        # create thumbnail
        video.save_frame(
            'thumbnail-{}-{}.jpg'.format(video_id, video_salt),
            t=0
        )

        # save cropped video to file
        video.write_videofile(
            "{}-{}.mp4".format(video_id, video_salt),
            fps=25,
            codec='libx264',
            audio_codec="aac"
        )

        video_path = os.path.join(
            settings.BASE_DIR, '{}-{}.mp4'.format(video_id, video_salt))
        thumbnail_path = os.path.join(
            settings.BASE_DIR,
            'thumbnail-{}-{}.jpg'.format(video_id, video_salt)
        )

        # upload cropped video to Instagram
        instagram.uploadVideo(
            video_path, thumbnail_path, caption=preview['caption'])
