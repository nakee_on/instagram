from django.contrib import admin
from django.urls import path

from video.views import VideoAPI, Instagram

urlpatterns = [
    path('admin/', admin.site.urls),
    path('video-list/', VideoAPI.as_view()),
    path('instagram/', Instagram.as_view()),
]
