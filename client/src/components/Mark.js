import React from 'react'
import PropTypes from 'prop-types'
import Icon from '@fortawesome/react-fontawesome'

const Mark = ({ value, onClick }) => {
  const handleOnClick = () => onClick(value)
  return (
    <button className="control-button" onClick={handleOnClick}>
      <Icon icon="map-marker" color="red" size="lg" />
    </button>
  )
}
Mark.propTypes = {
  value: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired,
}

export default Mark
