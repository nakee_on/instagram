import React from 'react'
import { ToastContainer, toast } from 'react-toastify'

import Player from 'components/Player'
import Button from 'components/Button'
import Spinner from 'components/Spinner'
import Preview from 'components/Preview'

import './App.css'

class App extends React.Component {
  state = {
    searchQuery: '',
    previousSearchQuery: null,
    requestLoading: false,
    isLoading: false,
    url: '',
    previews: [],
  }
  handleInputChange = event =>
    this.setState({ searchQuery: event.target.value })
  handleOnButtonClick = () => this.searchVideo()
  handleOnCrop = (preview) => {
    this.setState(state => ({
      previews: state.previews.concat({
        ...preview,
        id: state.previews.length,
        caption: '',
      }),
    }))
  }
  handleOnDelete = (id) => {
    this.setState(state => ({
      previews: state.previews.filter(o => o.id !== id),
    }))
  }
  handlePreviewCommentChange = (value, id) => {
    const index = this.state.previews.findIndex(o => o.id === id)

    if (index > -1) {
      const newPreviews = this.state.previews.concat()
      newPreviews[index].caption = value

      this.setState({ previews: newPreviews })
    }
  }
  handleCroppedVideoOnCrop = (preview, id) => {
    this.setState((state) => {
      const index = state.previews.findIndex(o => o.id === id)

      if (index > -1) {
        const newPreviews = state.previews.concat()
        newPreviews[index] = {
          ...preview,
          id,
        }
        return { previews: newPreviews }
      }

      return null
    })
  }
  skipVideo = async () => {
    const { url } = this.state
    const body = new FormData()
    body.append('url', url)

    await fetch('http://localhost:8000/video-list/', {
      method: 'PUT',
      body,
    })

    this.searchVideo()
  }
  searchVideo = async () => {
    const { searchQuery } = this.state
    if (searchQuery.length > 2) {
      this.setState({ isLoading: true })
      const body = new FormData()
      body.append('search_query', searchQuery)

      const response = await fetch('http://localhost:8000/video-list/', {
        method: 'POST',
        body,
      })

      const data = await response.json()

      this.setState({
        isLoading: false,
        url: data.url,
        previousSearchQuery: searchQuery,
      })
    }
  }
  handleDone = async () => {
    const { url, previews } = this.state

    this.setState({ requestLoading: true })

    const data = {
      previews,
      url,
    }

    const body = new FormData()
    body.append('data', JSON.stringify(data))

    await fetch('http://localhost:8000/instagram/', {
      method: 'POST',
      body,
    })

    toast.success('Готово!', {
      position: toast.POSITION.TOP_LEFT,
    })

    this.setState({ requestLoading: false, previews: [] })
  }
  render() {
    const {
      url,
      searchQuery,
      previousSearchQuery,
      isLoading,
      previews,
    } = this.state
    return (
      <div
        className="container-fluid"
        style={{ marginTop: 10, padding: '0 10px' }}
      >
        <ToastContainer />
        <div className="columns">
          <div className="column">
            <div className="columns">
              <div className="column is-3 is-offset-4">
                <input
                  type="text"
                  value={this.state.searchQuery}
                  onChange={this.handleInputChange}
                  placeholder="Поиск видео YouTube"
                  className="input"
                />
              </div>
              <div className="column is-narrow has-text-centered">
                {url &&
                searchQuery &&
                previousSearchQuery &&
                previousSearchQuery === searchQuery ? (
                  <Button
                    type="submit"
                    title="Следующее"
                    onClick={this.skipVideo}
                  />
                ) : (
                  <Button
                    isLoading={this.state.isLoading}
                    type="submit"
                    className="is-primary"
                    title="Поиск"
                    onClick={this.handleOnButtonClick}
                  />
                )}
              </div>
              <div className="column has-text-right-desktop has-text-centered-mobile">
                {previews.length > 0 && (
                  <Button
                    isLoading={this.state.requestLoading}
                    type="submit"
                    className="is-success"
                    title="Готово"
                    onClick={this.handleDone}
                  />
                )}
              </div>
            </div>
            <div className="field">
              {isLoading ? (
                <Spinner />
              ) : (
                <Player url={url} onCrop={this.handleOnCrop} />
              )}
            </div>
            <div className="field">
              <div className="columns is-multiline">
                {previews.map(preview => (
                  <div className="column is-half" key={preview.start}>
                    <Preview
                      id={preview.id}
                      onChange={this.handlePreviewCommentChange}
                    >
                      <Player
                        {...preview}
                        url={url}
                        isCropped
                        onDelete={this.handleOnDelete}
                        onCrop={this.handleCroppedVideoOnCrop}
                      />
                    </Preview>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default App
