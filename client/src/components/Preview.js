import React from 'react'
import PropTypes from 'prop-types'

const propTypes = {
  children: PropTypes.element.isRequired,
  onChange: PropTypes.func.isRequired,
  id: PropTypes.number.isRequired,
}

class Preview extends React.Component {
  state = {
    comment: '',
  }
  handleOnChange = (event) => {
    const { target: { value } } = event
    const { id, onChange } = this.props
    this.setState({ comment: value })

    onChange(value, id)
  }
  render() {
    const { children } = this.props
    return (
      <div className="card">
        <div className="card-image">
          <figure className="image">{children}</figure>
        </div>
        <div className="card-content">
          <div className="content">
            <textarea
              value={this.state.comment}
              onChange={this.handleOnChange}
              className="textarea"
              placeholder="Описание"
              style={{ backgroundColor: '#e2e2e2' }}
            />
          </div>
        </div>
      </div>
    )
  }
}

Preview.propTypes = propTypes

export default Preview
