import React from 'react'
import PropTypes from 'prop-types'
import Icon from '@fortawesome/react-fontawesome'
import Slider from 'rc-slider'
import 'rc-slider/assets/index.css'

const Controls = (props) => {
  const marksKeys = Object.keys(props.marks)
  const marksCount = marksKeys.length
  let addMarkButtonDisabled = false

  if (
    marksCount === 1 &&
    (Math.abs(props.progress - marksKeys[0]) >= 60 ||
    Math.abs(props.progress - marksKeys[0]) <= 3)
  ) {
    addMarkButtonDisabled = true
  }
  return (
    <div className="player-controls">
      <div className="player-controls-container">
        <div className="player-slider">
          <Slider
            step={0.1}
            min={props.start}
            max={props.duration}
            marks={props.marks}
            value={props.progress}
            onChange={props.onSliderChange}
            onAfterChange={props.onSliderAfterChange}
          />
        </div>
        <button className="control-button">
          <Icon
            icon={props.playing ? 'pause' : 'play'}
            color="#fff"
            onClick={props.onPlayToggle}
          />
        </button>
        <button className="control-button">
          <Icon
            icon={props.muted ? 'volume-off' : 'volume-up'}
            color="#fff"
            onClick={props.onMuteToggle}
          />
        </button>
        {props.marksControls && (
          <div className="controls-group">
            <button
              className="control-button"
              disabled={marksCount === 2 || addMarkButtonDisabled}
              onClick={props.onMarkCreate}
            >
              <Icon icon="plus-square" color="#fff" />
            </button>
            <button
              className="control-button"
              disabled={!props.markToRemove}
              onClick={props.onMarkRemove}
            >
              <Icon icon="minus-square" color="#fff" />
            </button>
            <button
              className="control-button"
              disabled={marksCount !== 2}
              onClick={props.onCrop}
            >
              <Icon icon="crop" color="#fff" />
            </button>
          </div>
        )}
      </div>
    </div>
  )
}

Controls.propTypes = {
  duration: PropTypes.number.isRequired,
  marks: PropTypes.object.isRequired, // eslint-disable-line
  progress: PropTypes.number.isRequired,
  onSliderChange: PropTypes.func.isRequired,
  onPlayToggle: PropTypes.func.isRequired,
  playing: PropTypes.bool.isRequired,
  markToRemove: PropTypes.number,
  start: PropTypes.number,
  muted: PropTypes.bool.isRequired,
  onMarkCreate: PropTypes.func,
  onMarkRemove: PropTypes.func,
  onSliderAfterChange: PropTypes.func.isRequired,
  onCrop: PropTypes.func,
  onMuteToggle: PropTypes.func.isRequired,
  marksControls: PropTypes.bool,
}

Controls.defaultProps = {
  markToRemove: null,
  marksControls: true,
  onMarkRemove: undefined,
  onMarkCreate: undefined,
  onCrop: undefined,
  start: 0,
}

export default Controls
