# Generated by Django 2.0.3 on 2018-03-28 07:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('video', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='is_viewed',
            field=models.BooleanField(default=False),
        ),
    ]
