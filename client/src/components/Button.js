import React from 'react'
import PropTypes from 'prop-types'
import FontAwesomeIcon from '@fortawesome/react-fontawesome'

const Button = ({
  className, isLoading, title, icon, ...props
}) => (
  <button
    className={`button ${className} ${isLoading ? 'is-loading' : ''}`}
    {...props}
  >
    {icon && (
      <span className="icon">
        <FontAwesomeIcon icon={icon} />
      </span>
    )}
    {title && <span>{title}</span>}
  </button>
)

Button.propTypes = {
  className: PropTypes.string,
  isLoading: PropTypes.bool,
  title: PropTypes.string,
  icon: PropTypes.string,
}

Button.defaultProps = {
  className: 'button',
  isLoading: false,
  icon: null,
  title: null,
}

export default Button
