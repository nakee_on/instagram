import json
import re
import urllib.parse
import urllib.request

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Video
from .serializer import VideoSerializer
from .tasks import crop_and_deploy


class VideoAPI(APIView):
    def post(self, request, **kwargs):
        search_query = request.data.get('search_query')
        query_string = urllib.parse.urlencode({"search_query": search_query})
        html_content = urllib.request.urlopen(
            "http://www.youtube.com/results?" + query_string)
        search_results = re.findall(
            r'href=\"\/watch\?v=(.{11})', html_content.read().decode())

        for result in search_results:
            url = 'https://www.youtube.com/watch?v=%s' % result

            if Video.objects.filter(url=url, is_viewed=True).exists():
                continue
            elif Video.objects.filter(url=url, is_viewed=False).exists():
                video = Video.objects.get(url=url)
            else:
                video = Video.objects.create(url=url)

            serializer = VideoSerializer(video)

            return Response(serializer.data)

        return Response(status=status.HTTP_204_NO_CONTENT)

    def put(self, request, **kwargs):
        url = request.data.get('url')

        try:
            video = Video.objects.get(url=url)
        except Video.DoesNotExist:
            return Response(status=status.HTTP_204_NO_CONTENT)

        video.is_viewed = True
        video.save()

        return Response(status=status.HTTP_204_NO_CONTENT)

    def delete(self, request):
        pass


class Instagram(APIView):
    def post(self, request):
        data = request.data.get('data')
        data = json.loads(data)
        previews = data.get('previews')
        video_url = data.get('url')

        crop_and_deploy.delay(video_url, previews)

        return Response(status=status.HTTP_204_NO_CONTENT)
