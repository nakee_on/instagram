from django.db import models


class Video(models.Model):
    url = models.CharField(max_length=256)
    is_viewed = models.BooleanField(default=False)

    def __str__(self):
        return self.url
